import argparse
import numpy as np
import cv2 as cv
import pathlib
import os

show_work_path = ""
step = 1


def write_intermediate_image(image, name):
    global step
    cv.imwrite(show_work_path + "/step-" + str(step) + "-" + name, image)
    step += 1


def find_clock_face(image, show_work=False):
    color_image = image
    grey_image = cv.cvtColor(image,  cv.COLOR_RGB2GRAY)
    blurred_image = cv.medianBlur(grey_image, 5)
    circles = cv.HoughCircles(blurred_image, cv.HOUGH_GRADIENT_ALT, 2, 20, param1=300, param2=0.9)
    circles = np.uint16(np.around(circles))

    if show_work:
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(color_image, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv.circle(color_image, (i[0], i[1]), 2, (0, 0, 255), 3)

        write_intermediate_image(color_image, "clock_circle.jpg")

    return circles


def crop(image, center, radius):
    start_x = center[0] - radius
    start_y = center[1] - radius

    return image[start_y:start_y + (radius * 2), start_x: start_x+(radius*2)]


def area_of_contour_bounding_box(contour):
    x, y, w, h = cv.boundingRect(contour)
    return w*h


def contour_width(contour):
    x, y, w, h = cv.boundingRect(contour)
    return w


def hand_y_location(contour):
    x, y, w, h = cv.boundingRect(contour)
    return y + (h/2)


def get_hand_y_coords_from_contours(contours):

    if len(contours) == 1:
        contours.append(contours[0])

    sorted_by_width = sorted(contours, reverse=True, key=contour_width)

    minute = hand_y_location(sorted_by_width[0])
    hour = hand_y_location(sorted_by_width[1])

    return (hour, minute)


def y_to_minute(y, height):
    normed_y_coord = (float(height) - float(y)) / float(height)
    minutes = (normed_y_coord * 61) - 1
    return round(minutes)


def y_to_hour(y, height):
    normed_y_coord = (float(height) - float(y)) / float(height)
    hours = (normed_y_coord * 12)
    return round(hours)


def adjust_hour(hour, minute):

    if minute >= 34:
        hour = hour - 1

    if hour == 0:
        hour = 12

    if hour == -1:
        hour = 11

    return(hour, minute)


def get_hand_positions(image, show_work):
    color_image = cv.rotate(image, cv.ROTATE_180)
    gray_image = cv.cvtColor(color_image, cv.COLOR_RGB2GRAY)

    # This feels a little more art than science, but we are going to do a
    # erosion pass to remove extraneous details
    kernel = np.ones((1, 150), np.uint8)
    di_image = cv.dilate(gray_image, kernel, iterations=1)
    er_image = cv.erode(di_image, kernel, iterations=1)
    negative = cv.bitwise_not(er_image)

    edges = cv.Canny(negative, 100, 150)

    drawn_lines = er_image.copy()
    drawn_lines = cv.cvtColor(drawn_lines, cv.COLOR_GRAY2RGB)

    # Just to be sure, we disconnect the edges from the border of the image
    (height, width) = edges.shape
    edges = cv.line(edges, (width-1, 0), (width-1, height-1), (0, 0, 0), 5)

    # find contours, representing the individual hands
    contours, hierarchy = cv.findContours(edges, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    # sort by area to find the contours that are most likely the hands
    sorted_contours = sorted(contours, reverse=True, key=area_of_contour_bounding_box)
    
    # discard anything that is not a hand
    if len(sorted_contours) > 2:
        sorted_contours = sorted_contours[0:2]

    (hour, minute) = get_hand_y_coords_from_contours(sorted_contours)

    if show_work:

        line_drawing = color_image.copy()
        for contour in sorted_contours:
            x, y, w, h = cv.boundingRect(contour)
            line_drawing = cv.rectangle(line_drawing, (x, y), (x+w, y+h), (0, 0, 255), 5)

        write_intermediate_image(di_image, "dilated_image.jpeg")
        write_intermediate_image(er_image, "eroded_image.jpeg")
        
        write_intermediate_image(negative, "negative.jpeg")
        write_intermediate_image(edges, "edges.jpeg")
        write_intermediate_image(line_drawing, "contour_bounding_box.jpeg")
   
    return(hour, minute)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Clockwatcher looks at an image of an analog clock, and attempts to deduce the time")
 
    parser.add_argument('clock_image', type=str, help="path to image of an analog clock")
    parser.add_argument('--show_work', help="create images that demonstrate intermediate steps", action='store_true')
    args = parser.parse_args()

    # in the event the user wants us to show the work, we need a place to dump intermediate images
    if args.show_work:
        file_path = pathlib.Path(args.clock_image)
        file_stem = file_path.stem

        show_work_path = os.getcwd()+"\\"+file_stem

        # wipe out previous artifacts from the same input image 
        if os.path.isdir(show_work_path):
            for file in os.listdir(show_work_path):
                os.remove(show_work_path + '\\' + file)

            os.rmdir(show_work_path)

        os.mkdir(show_work_path)

    # pull in the user specified image
    image = cv.imread(args.clock_image, 1)

    # Use HoughCircles to locate the permiter of the clock
    circles = find_clock_face(image.copy(), args.show_work)
    # crop clock
    circle = circles[0][0]
    cropped_image = crop(image, (circle[0], circle[1]), circle[2])
    if(args.show_work):
        write_intermediate_image(cropped_image, "cropped_clock_face.jpg")

    # rotate image 90deg clockwise
    rotated_image = cv.rotate(cropped_image, cv.ROTATE_90_CLOCKWISE)
    if args.show_work:
        write_intermediate_image(rotated_image, "rotated_cropped_clock_face.jpeg")

    # Apply Warp Polar to "unwrap" the circle
    (rows, cols, _) = rotated_image.shape
    unwrapped_image = cv.warpPolar(rotated_image, (rows, cols), (circle[2], circle[2]), circle[2], cv.WARP_POLAR_LINEAR)
    if args.show_work:
        write_intermediate_image(unwrapped_image, "unwrapped_clock_face.jpeg")

    # use contour detection with erosion to differentiate between hour and minute hands
    (hour_position, minute_position) = get_hand_positions(unwrapped_image, args.show_work)

    # use the position of the hands to determine time
    minute = y_to_minute(minute_position, rows)
    hour = y_to_hour(hour_position, rows)

    (hour, minute) = adjust_hour(hour, minute)

    print("The clock reads " + str(hour) + ":" + str(minute))