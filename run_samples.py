import os
import pathlib
import subprocess

image_dir = pathlib.Path( "inputs")


for dir in os.listdir(image_dir):
    current_dir = image_dir / pathlib.Path(dir)
    for image in os.listdir(current_dir):
        file = current_dir / pathlib.Path(image)

        command = f"python  src/clockwatcher.py {file}"

        output = subprocess.check_output(command , universal_newlines= True)
        print(f" file: {dir}/{file.name}, output:{output[:-1]}")