# ClockWatcher-CS-583

This is the code portion of the CS  583 Intro To computer Vision  final project

# Setup 
1. Clone this repo
2. install opencv-python with the command `pip install opencv-python`
3. run `python3 src/clockwatcher.py inputs/computer_generated/9_26.jpeg`


# Show Intermediate Steps
if you want to see the intermediate steps, append `--show_work` to the command you are running. A directory named after the image you process will be generated and filled with the images generated as clockwatcher runs.

# Run ClockWatcher against all sample data
running the `run_samples.py` script will run ClockWatcher against every file in every directory in `inputs`

# Dependencies
1. OpenCV
